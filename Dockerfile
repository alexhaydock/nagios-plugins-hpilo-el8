# Build inside Rocky Linux 8 container. We can't build inside
# a RHEL UBI container since the rpmdevtools package is available
# inside AppStream for full RHEL, but seemingly not for UBI.
FROM docker.io/library/rockylinux:8

# Install RPM packaging tools and set up rpmbuild dev tree
RUN dnf install -y rpmdevtools rpmlint dnf-plugins-core
RUN rpmdev-setuptree

# Copy SOURCES and SPEC into container
COPY nagios-plugins-hpilo-1.5.0.zip /root/rpmbuild/SOURCES/
COPY nagios-plugins-hpilo.spec /root/rpmbuild/SPECS/

# Copy entrypoint script into container
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Change into SPECS dir
WORKDIR /root/rpmbuild/SPECS

# Install dependencies for this build
RUN dnf builddep -y nagios-plugins-hpilo.spec

# Build SRPM
RUN rpmbuild -bs nagios-plugins-hpilo.spec

# Build RPM
RUN rpmbuild -bb nagios-plugins-hpilo.spec

# Run the entrypoint (build script) when this container runs
# This will output the built SRPM and RPM into a directory mounted as /host
CMD ["/entrypoint.sh"]
