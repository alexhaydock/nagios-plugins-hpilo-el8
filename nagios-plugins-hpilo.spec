Name:           nagios-plugins-hpilo
# The version spec here is a bit of an issue. This is based on the version
# tag in the poorly-maintained upstream repo:
#
#    https://github.com/HewlettPackard/nagios-plugins-hpilo
#
# Security fixes have been recently (Jul 2020) merged into the default
# branch of the repo above (1.5.0). It seems like the highest versioned
# branch is actually 1.5.1 but has older code in it than the default branch.
Version:        1.5.0
Release:        1%{?dist}
Summary:        RHEL 8 package for Nagios plug-in for iLO Agentless Management

License:        GPLv2
URL:            https://github.com/HewlettPackard/nagios-plugins-hpilo
Source0:        %{name}-%{version}.zip

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  gcc
BuildRequires:  glibc-devel
BuildRequires:  make
BuildRequires:  net-snmp-devel
BuildRequires:  nmap
BuildRequires:  unzip

Requires:       net-snmp
Requires:       nmap

%description
Nagios plug-in for iLO Agentless Management

%prep
%setup -q

%build
%configure
%make_build

%install
# Please note that this is the OLD makeinstall macro that the RPM build tools
# keep around for "old/broken packages that don't support DESTDIR properly"
# which is (naturally) us, since the Makefiles in the hpilo repo are ancient.
#
# https://github.com/rpm-software-management/rpm/blob/f0c158cbc8a50a776b44de2c0fe744c451155a41/macros.in#L1035-L1054
#
# If the upstream updates, then we might need to switch to make_install instead.
%makeinstall

%files
%license COPYING
%doc README
%{_libexecdir}/hpeilo_nagios_config
%{_libexecdir}/hpeilo/Base.css
%{_libexecdir}/hpeilo/CPU_Info.js
%{_libexecdir}/hpeilo/Fan_Info.js
%{_libexecdir}/hpeilo/Memory_Info.js
%{_libexecdir}/hpeilo/nagios_hpeilo_engine
%{_libexecdir}/hpeilo/nagios_hpeilo_service_details.php
%{_libexecdir}/hpeilo/Network_Info.js
%{_libexecdir}/hpeilo/PS_Info.js
%{_libexecdir}/hpeilo/Storage_Info.js
%{_libexecdir}/hpeilo/Temp_Info.js
%{_libexecdir}/nagios_hpeilo_cfg_generator
%{_libexecdir}/nagios_hpeilo_engine
%{_libexecdir}/nagios_hpeilo_traps
%{_prefix}/conf.d/generic-contact_nagios2.cfg

%changelog
* Tue Jan 04 2022 Alex Haydock <alex@alexhaydock.co.uk> 1.5.0-1
- First release
