#!/bin/sh
set -xeu

# Build container
podman build -t hpeilo .

# Mount local build/ directory into container so we can output our build content into it
mkdir -p "${PWD}/build"
podman run --rm -it -v "${PWD}/build:/host:rw,Z" localhost/hpeilo
