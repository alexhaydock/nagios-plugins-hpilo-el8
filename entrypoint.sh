#!/bin/sh
set -xeu

# Copy built assets into host directory
mkdir -p /host
cp -fv ${HOME}/rpmbuild/SRPMS/*.src.rpm /host/
cp -fv ${HOME}/rpmbuild/RPMS/x86_64/*.rpm /host/
